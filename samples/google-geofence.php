<?
$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

    <title>Google Geofence Interactive Map</title>
      <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyASx_MoPbDhJLXvAfGBLyfnB36WRW3o06s"
      type="text/javascript"></script>

    <script type="text/javascript">
    //<![CDATA[
    var map;
    var polygon;
    var pt;

    function replace(str,to,from)
    {
    var idx = str.indexOf( from );
    while ( idx > -1 ) {
        str = str.replace( from, to );
        idx = str.indexOf( from );
    }
    return str;
   }

   function getHeight()
   {
    return(document.getElementById("symbol").height);
   }
   function getWidth()
   {
    return(document.getElementById("symbol").width);
   }

   function getLat()
   {
       return(pt.y);
   }
   function getLon()
   {
       return(pt.x);
   }

   function addMarker(lat,lon, iconImage)
   {
        var point = new GLatLng(lat,lon);
        var marker = new GMarker(point, {title: "abc"});
        map.addOverlay(marker);
   }

    /**
     * Add a circle to the global variable "map". This function won't work for circles that encompass
     * the North or South Pole. Also, there is a slight distortion in the upper-left, upper-right,
     * lower-left, and lower-right sections of the circle that worsens as it gets larger and/or closer
     * to a pole.
     * @param lat Latitude in degrees
     * @param lng Longitude in degrees
     * @param radius Radius of the circle in statute miles
     * @param {String} strokeColor Color of the circle outline in HTML hex style, e.g. "#FF0000"
     * @param strokeWidth Width of the circle outline in pixels
     * @param strokeOpacity Opacity of the circle outline between 0.0 and 1.0
     * @param {String} fillColor Color of the inside of the circle in HTML hex style, e.g. "#FF0000"
     * @param fillOpacity Opacity of the inside of the circle between 0.0 and 1.0
     */
    function drawCircle(lat, lng, radius, strokeColor, strokeWidth, strokeOpacity, fillColor, fillOpacity) {
      var d2r = Math.PI/180;
      var r2d = 180/Math.PI;
      var Clat = radius * 0.014483;  // Convert statute miles into degrees latitude
      var Clng = Clat/Math.cos(lat*d2r);
      var Cpoints = [];
      for (var i=0; i < 33; i++) {
        var theta = Math.PI * (i/16);
        Cy = lat + (Clat * Math.sin(theta));
        Cx = lng + (Clng * Math.cos(theta));
        var P = new GPoint(Cx,Cy);
        Cpoints.push(P);
      }
      if (polygon) {
         map.removeOverlay(polygon);
      }
      polygon = new GPolygon(Cpoints, strokeColor, strokeWidth, strokeOpacity, fillColor, fillOpacity);
      map.addOverlay(polygon);
     }

   function load(lat,lon, iconImage)
   {
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map"));
        map.addControl(new GScaleControl());
        map.enableDoubleClickZoom();
        map.addControl(new GOverviewMapControl());
        //map.addControl(new GSmallMapControl());
        map.addControl(new GLargeMapControl());
        map.addControl(new GMapTypeControl());
        map.setMapType(G_HYBRID_MAP);
        map.setCenter(new GLatLng(lat,lon), 15);


        // Create our "tiny" marker icon
        var icon = new GIcon();
        icon.image = iconImage;
        //icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
        icon.iconSize = new GSize(getWidth(), getHeight());
        //icon.iconSize = new GSize();
        //icon.shadowSize = new GSize(22, 20);
        icon.iconAnchor = new GPoint(6, 6);
        //icon.infoWindowAnchor = new GPoint(10, 10);

        var point = new GLatLng(lat,lon);


        var marker = new GMarker(point,  {title: "Asset Latest Location", icon:icon});


        GEvent.addListener(marker, "click", function() {
          //marker.openInfoWindowTabsHtml(infoTabs);
        });
        map.addOverlay(marker);

        //marker.openInfoWindowTabsHtml(infoTabs);

        var marker2 = new GMarker(point, {draggable: true, title: "GeoFence Center"});
        var ptStr ="("+(Math.round(lat*100000)/100000)+", "+(Math.round(lon*100000)/100000)+")";
        drawCircle(lat,lon, 0.3125, "#000080", 1, 0.75, "#0000FF",0.5);

        GEvent.addListener(marker2, "dragstart", function() {
          map.closeInfoWindow();
          });

        GEvent.addListener(marker2, "dragend", function() {

          pt = marker2.getPoint();
          ptY = (Math.round(pt.y*100000)/100000);
          ptX = (Math.round(pt.x*100000)/100000);

          var ptStr ="("+ptY+", "+ptX+")";
          var dist = point.distanceFrom(pt);
          var infoStr = Math.round(dist)+" m";
          drawCircle(pt.y, pt.x, 0.3125, "#000080", 1, 0.75, "#0000FF",0.5);
          if (dist>1609)
          {
            var miles = Math.round(dist/1609 * 100)/100;
            var kms= Math.round(dist/10)/100;

            infoStr = miles+" miles = "+kms+" Km";
          }
          marker2.openInfoWindowHtml("<font face=arial size='-2'>Distance from Asset: "+infoStr+"<br>GPS: <b>"+ptStr+"</b></font>");
          document.command.latitude.value = ptY;
          document.command.longitude.value =  ptX;

          });


        map.addOverlay(marker2);

      }
    }
 //]]>
    </script>
  </head>

  <?


  if ($latitude=="") $latitude=0;
  if ($longitude=="") $longitude=0;
  $gfIcon = "trailer.gif";
  if ($symbol=="")
    $symbol = $gfIcon;

  print("<body onload=load($latitude,$longitude,\"$symbol\") onunload=GUnload()>");



  print("<img id='symbol' src='$symbol'/>");

    print("Name: <b>".$name."</b>, Fleet: ".$fleet);

    print("<form  method='post' name='command' action=''>");

	print("<input type='hidden' name='asset' value='$AdC'>\n");
	print("Geofene Center:");
	print("<input type='text' value='$latitude' name='latitude'>&nbsp;&nbsp;");
	print("<input type='text' value='$longitude' name='longitude'>");

 ?>
  <br>
	Geofence ID:&nbsp;&nbsp;
	<select class="input" name='targetId'>
	<option value='0'>0</option>
	<option value='1'>1</option>
	<option value='2'>2</option>
	<option value='3'>3</option>
	<option value='4'>4</option>
	<option value='5'>5</option>
	<option value='6'>6</option>
	<option value='7'>7</option>
	</select>
	<input class="input"  type="submit" value="Set Geofence." name="type">
	<input class="input"  type="submit" value="Disable Geofence." name="type">
	</form>
	<hr>
	<table><tr><td>
	<div id="map" style="width: 700px; height: 500px"></div>
    </td>

<?
    print("<td>\n<table  border='1' width='100%' cellpading=0  cellspacing=0><thead>\n");

    print("<tr><th nowrap>Fence Id</th><th>Date ($tzStr)</th>\n");
	print("<th nowrap>Fence Center</th></tr></thead>\n");

    $fences = array("Not Defined","Not Defined","Not Defined","Not Defined","Not Defined","Not Defined","Not Defined","Not Defined");
    $fencesDT = array("Not Defined","Not Defined","Not Defined","Not Defined","Not Defined","Not Defined","Not Defined","Not Defined");


 for ($j = 0; $j < 8; $j++)
 {
    print("<tr><td>$j</td><td>".$fencesDT[$j]."</td><td>".$fences[$j]."\n");
    $targetFence = $fencesGPS[$j];
    if (strlen($fences[$j])>25)
      print("<script type=\"text/javascript\"> addMarker($targetFence); </script>\n</td></tr>");

 }
 print("</table></td>");

?>
  </tr></table>

  </body>

</html>

