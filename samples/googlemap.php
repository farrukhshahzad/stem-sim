 <?
$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];
$label = $_GET['label'];
$address = $_GET['address'];
$address2 = $_GET['address2'];
//longitude=50.15&latitude=26.3&label=Farrukh_Shahzad&address=16002_Clayton_Green_Dr._&address2=Houston_TX_77082
if ($latitude=="") $latitude = 50.15;
if ($longitude=="") $longitude = 26.3;
if ($label=="") $label = "DONS Headquarter";
if ($address=="") $address = "KFUPM";
if ($address2=="") $address2 = "Dhahran-31261, KSA";
 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>DONS Map <? echo $label; ?></title>

<!--	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=ABQIAAAAjl1r-5HYdskx544zI-06SBSc9zuSKdXIvHhGmKpV1ReIN1tAYhRd7dkS02D-fqLdRVoH-aFD9_uHww" type="text/javascript"></script>
-->
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key= AIzaSyASx_MoPbDhJLXvAfGBLyfnB36WRW3o06s" type="text/javascript"></script>

 
    <script type="text/javascript">
    //<![CDATA[
    function replace(str,to,from)
    {
    var idx = str.indexOf( from );
    while ( idx > -1 ) {
        str = str.replace( from, to );
        idx = str.indexOf( from );
    }
    return str;
   }

   function getHeight()
   {
    return(document.getElementById("symbol").height);
   }
   function getWidth()
   {
    return(document.getElementById("symbol").width);
   }

   function load(lat,lon,asset,address,address2, other,iconImage)
   {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GScaleControl());
        map.enableDoubleClickZoom();
        map.addControl(new GOverviewMapControl());
        //map.addControl(new GSmallMapControl());
        map.addControl(new GLargeMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(lat,lon), 15);


        // Create our "tiny" marker icon
        var icon = new GIcon();
        icon.image = iconImage;
        //icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
        icon.iconSize = new GSize(getWidth(), getHeight());
        //icon.iconSize = new GSize();
        //icon.shadowSize = new GSize(22, 20);
        icon.iconAnchor = new GPoint(6, 6);
        icon.infoWindowAnchor = new GPoint(10, 10);

        var point = new GLatLng(lat,lon);

        // Our info window content
        unit = "<font face=arial size='-2'>Name: <b>"+replace(asset," ","_")+"</b><br>Address: <b>"+replace(address," ","_")+"<br>"+replace(address2," ","_")+"</b></font>";
        gps = "<br><font face=arial size='-2'>GPS: <b>"+lat+", "+lon+"</b></font>";

        if (other=="")
        {
           var infoTabs = [
          new GInfoWindowTab("Location",unit+gps),
          ];
        }
        else
        {
                other = replace(other,"<","~");
		        other = replace(other,">","`");
		        other = replace(other," ","|");

           var infoTabs = [
          new GInfoWindowTab("Location",unit+gps),
          new GInfoWindowTab("Other Info", "<font face=arial size='-2'>"+replace(other," ","_")+"</font>")
          ];
        }
        var marker = new GMarker(point, icon);


        GEvent.addListener(marker, "click", function() {
          marker.openInfoWindowTabsHtml(infoTabs);
        });
        map.addOverlay(marker);

        marker.openInfoWindowTabsHtml(infoTabs);

        var marker2 = new GMarker(point, {draggable: true, title: "Drag Me!"});

        GEvent.addListener(marker2, "dragstart", function() {
          map.closeInfoWindow();
          });

        GEvent.addListener(marker2, "dragend", function() {
          pt = marker2.getPoint();
          var ptStr ="("+(Math.round(pt.y*10000)/10000)+", "+(Math.round(pt.x*10000)/10000)+")";
          var dist = point.distanceFrom(pt);
          var infoStr = Math.round(dist)+" m";
          if (dist>1609)
          {
            var miles = Math.round(dist/1609 * 100)/100;
            var kms= Math.round(dist/10)/100;

            infoStr = miles+" miles = "+kms+" Km";
          }
          marker2.openInfoWindowHtml("<font face=arial size='-2'>Distance from Asset: "+infoStr+"<br>GPS: <b>"+ptStr+"</b></font>");
          });


        map.addOverlay(marker2);
        //marker2.openInfoWindowHtml("Drag Me!");

        // Add 10 markers to the map at random locations
        //var bounds = map.getBounds();
        //var southWest = bounds.getSouthWest();
        //var northEast = bounds.getNorthEast();
        //var lngSpan = northEast.lng() - southWest.lng();
        //var latSpan = northEast.lat() - southWest.lat();
        //for (var i = 0; i < 10; i++) {
        //  var point = new GLatLng(southWest.lat() + latSpan * Math.random(),
        //                          southWest.lng() + lngSpan * Math.random());
        //  map.addOverlay(new GMarker(point, icon));
        //}
      }
    }
 //]]>
    </script>
  </head>

  <?



  if ($latitude=="") $latitude=0;
  if ($longitude=="") $longitude=0;
  //$asset = $label.",".$dateTime;
  if ($symbol=="")
    $symbol = "trailer.gif";

//print("<body onload=load($latitude,$longitude,\"$label\",\"$fleet\",\"$dateTime\",\"$place\",\"$spHead\",\"$other\",\"$symbol\") onunload=GUnload()>");

print("<body onload=load($latitude,$longitude,\"$label\",\"$address\",\"$address2\",\"$other\",\"$symbol\") onunload=GUnload()>");


  ?>
    <div id="map" style="width: 800px; height: 500px"></div>

    <?
    print("<img id='symbol' src='$symbol'/>");
    print("Name: <b>".str_replace("_"," ",$label)."</b>");
    ?>


</body>

</html>
