#STEM-SIM

Interactive JavaScript/HTML5/SVG based STEM simulator Apps.

See the following examples/samples:
http://phet.colorado.edu/en/simulation/legacy/calculus-grapher
http://phet.colorado.edu/en/simulation/legacy/pendulum-lab
http://phet.colorado.edu/en/simulation/legacy/torque
http://phet.colorado.edu/en/simulation/legacy/density

The idea is to later build similar interactive STEM simulation Apps using latest mobile-friendly HTML5/SVG based JS libraries like D3.js, Highcharts, Go.js, etc.


